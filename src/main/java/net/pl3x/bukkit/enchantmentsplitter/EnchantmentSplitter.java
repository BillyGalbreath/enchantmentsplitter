package net.pl3x.bukkit.enchantmentsplitter;

import net.pl3x.bukkit.enchantmentsplitter.command.CmdEnchantmentSplitter;
import net.pl3x.bukkit.enchantmentsplitter.configuration.Config;
import net.pl3x.bukkit.enchantmentsplitter.configuration.Lang;
import net.pl3x.bukkit.enchantmentsplitter.listener.AnvilListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class EnchantmentSplitter extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        Bukkit.getPluginManager().registerEvents(new AnvilListener(this), this);

        getCommand("enchantmentsplitter").setExecutor(new CmdEnchantmentSplitter(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static EnchantmentSplitter getPlugin() {
        return EnchantmentSplitter.getPlugin(EnchantmentSplitter.class);
    }
}
