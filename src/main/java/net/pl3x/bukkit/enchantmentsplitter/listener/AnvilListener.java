package net.pl3x.bukkit.enchantmentsplitter.listener;

import net.pl3x.bukkit.enchantmentsplitter.EnchantmentSplitter;
import net.pl3x.bukkit.enchantmentsplitter.Logger;
import net.pl3x.bukkit.enchantmentsplitter.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class AnvilListener implements Listener {
    private final EnchantmentSplitter plugin;
    private final Random random = new Random();

    public AnvilListener(EnchantmentSplitter plugin) {
        this.plugin = plugin;
    }

    /*
     * Called when an item is put in a slot for repair by an anvil
     */
    @EventHandler
    public void onItemInsertAnvil(PrepareAnvilEvent event) {
        InventoryView view = event.getView();
        if (view.getType() != InventoryType.ANVIL) {
            Logger.debug("Not an anvil");
            return; // not an anvil
        }

        ItemStack item1 = view.getItem(1);

        // check if splitting
        if (item1.getType() != Material.BOOK) {
            Logger.debug("Not splitting");
            return; // not splitting
        }

        ItemStack item0 = view.getItem(0);
        Map<Enchantment, Integer> enchantments = new HashMap<>();

        // get enchants from enchanted book
        if (item0.getType() == Material.ENCHANTED_BOOK) {
            enchantments.putAll(((EnchantmentStorageMeta) item0.getItemMeta()).getStoredEnchants());
        }

        // get enchants from gear
        else {
            enchantments.putAll(item0.getEnchantments());
        }

        // check if splitting
        if (enchantments.isEmpty() || enchantments.size() <= 1) {
            Logger.debug("Nothing to split");
            return; // nothing to split
        }

        // pick random enchantment to split out
        Enchantment enchantment = null;
        int level = 1;
        int i = 0;
        int picked = random.nextInt(enchantments.size());
        for (Map.Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
            if (i == picked) {
                enchantment = entry.getKey();
                level = entry.getValue();
                break;
            }
            i++;
        }

        // double check
        if (enchantment == null) {
            Logger.warn("Something went wrong trying to split enchantment");
            return;
        }

        // build new enchantment book from picked enchantment
        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
        ItemMeta newMeta = book.getItemMeta();
        ((EnchantmentStorageMeta) newMeta).addStoredEnchant(enchantment, level, false);
        ((Repairable) newMeta).setRepairCost(0); // just to be sure
        book.setItemMeta(newMeta);
        book.setAmount(1);
        event.setResult(book);

        // set split cost
        setRepairCost(view);

        // update inventory to get rid of ghosted items
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            for (HumanEntity viewer : event.getViewers()) {
                //noinspection deprecation
                ((Player) viewer).updateInventory();
            }
        }, 1);

        // set split cost again (to fix out of sync issues :S
        Bukkit.getScheduler().runTaskLater(plugin,
                () -> setRepairCost(view), 2);
    }

    /*
     * Called when player pulls item from anvil result slot
     */
    @EventHandler
    public void onSplitResultClick(InventoryClickEvent event) {
        InventoryView view = event.getView();
        if (view.getType() != InventoryType.ANVIL) {
            return; // not an anvil
        }
        if (event.getSlot() != 2) {
            Logger.debug("Not clicking result");
            return; // not clicking result slot
        }

        // check if result is enchanted book
        ItemStack result = view.getItem(2);
        if (result.getType() != Material.ENCHANTED_BOOK) {
            Logger.debug("Not making enchantment book");
            return; // not making an enchanted book
        }

        // check if splitting
        ItemStack item1 = view.getItem(1);
        if (item1.getType() != Material.BOOK) {
            Logger.debug("Not splitting");
            return; // not splitting
        }

        // get result's meta
        Map<Enchantment, Integer> resultEnchants = ((EnchantmentStorageMeta) result.getItemMeta()).getStoredEnchants();
        if (resultEnchants == null || resultEnchants.isEmpty() || resultEnchants.size() > 1) {
            Logger.debug("Result has wrong number of enchants");
            return; // result has wrong number of enchants
        }

        // check result book enchantment is from recipe
        Enchantment resultEnchantment = null;
        int resultLevel = 1;
        for (Map.Entry<Enchantment, Integer> entry : resultEnchants.entrySet()) {
            resultEnchantment = entry.getKey();
            resultLevel = entry.getValue();
        }

        ItemStack item0 = view.getItem(0);
        Map<Enchantment, Integer> sourceEnchants;

        // get source enchants from book
        if (item0.getType() == Material.ENCHANTED_BOOK) {
            sourceEnchants = ((EnchantmentStorageMeta) item0.getItemMeta()).getStoredEnchants();
        }

        // get source enchants from gear
        else {
            sourceEnchants = item0.getEnchantments();
        }

        if (sourceEnchants.isEmpty() || sourceEnchants.size() == 1) {
            Logger.debug("Not splitting");
            return; // not splitting
        }

        if (!sourceEnchants.containsKey(resultEnchantment)) {
            Logger.debug("Source does not contain that enchantment");
            return; // source doesnt contain result's enchantment
        }

        if (sourceEnchants.get(resultEnchantment) != resultLevel) {
            Logger.debug("Source and result different levels");
            return; // source and result levels are different
        }

        // check cost
        Logger.debug("Repair cost: " + Config.SPLIT_COST);
        Player player = (Player) event.getWhoClicked();
        if (player.getGameMode() != GameMode.CREATIVE) {
            if (Config.SPLIT_COST > player.getLevel()) {
                event.setCancelled(true);
                //noinspection deprecation
                player.updateInventory();
                Logger.debug("Player does not have enough exp levels: " + Config.SPLIT_COST + " > " + player.getLevel());
                Bukkit.getScheduler().runTaskLater(plugin,
                        () -> setRepairCost(view), 1);
                return; // player does not have enough exp levels
            }

            player.setLevel(player.getLevel() - Config.SPLIT_COST);
        }

        // split the enchantment from the source
        ItemMeta sourceMeta = item0.getItemMeta();
        if (item0.getType() == Material.ENCHANTED_BOOK) {
            ((EnchantmentStorageMeta) sourceMeta).removeStoredEnchant(resultEnchantment);
        } else {
            sourceMeta.removeEnchant(resultEnchantment);
        }

        // store the anvil block (to check if it breaks)
        Block anvil = event.getClickedInventory().getLocation().getBlock();

        // apply the split
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            // decrement the book
            ItemStack book = item1;
            if (book.getAmount() > 1) {
                book.setAmount(book.getAmount() - 1);
            } else {
                book = new ItemStack(Material.AIR);
            }

            // remove enchantment from source
            item0.setItemMeta(sourceMeta);

            // check if anvil broke! drop source and books
            if (anvil.getState().getType() != Material.ANVIL) {
                anvil.getWorld().dropItem(anvil.getLocation(), item0);
                if (book.getType() != Material.AIR) {
                    anvil.getWorld().dropItem(anvil.getLocation(), book);
                }
                // player already has the result
                return;
            }

            // update the anvil inventory
            view.setItem(1, book);
            view.setItem(0, item0);
        }, 1);
    }

    private void setRepairCost(InventoryView view) {
        ((AnvilInventory) view.getTopInventory()).setRepairCost(Config.SPLIT_COST);
    }
}
